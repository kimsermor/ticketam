package com.fides.ticketing.models;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import io.github.millij.poi.ss.model.annotations.Sheet;
import io.github.millij.poi.ss.model.annotations.SheetColumn;

@Document(collection = "remedy")
@Sheet
public class Ticket {

	@Id
	private String id;

	@SheetColumn("Id Univoco Richiesta")
	private String idUnivocoRichiesta;
	
	@SheetColumn("Stato Richiesta")
	private String stato;

	@SheetColumn("Descrizione")
	private String descrizione;

	@SheetColumn("Note")
	private String note;
	
	@SheetColumn("APPLICAZIONE")
	private String applicazione;

	@SheetColumn("Codice Applicazione")
	private String codiceApplicazione;

	@SheetColumn("CHIAVE1")
	private String chiave1;

	@SheetColumn("CHIAVE2")
	private String chiave2;

	@SheetColumn("CHIAVE3")
	private String chiave3;

	@SheetColumn("CHIAVE4")
	private String chiave4;

	@SheetColumn("CHIAVE5")
	private String chiave5;

	@SheetColumn("CHIAVE6")
	private String chiave6;

	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)	
	private Date dataInserimento;
	
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private Date dataAggiornamento;

	public Ticket() {
		super();
	}

	public Ticket(String id, String idUnivocoRichiesta, String stato, String descrizione, String note,
			String applicazione, String codiceApplicazione, String chiave1, String chiave2, String chiave3,
			String chiave4, String chiave5, String chiave6, Date dataInserimento, Date dataAggiornamento) {
		super();
		this.id = id;
		this.idUnivocoRichiesta = idUnivocoRichiesta;
		this.stato = stato;
		this.descrizione = descrizione;
		this.note = note;
		this.applicazione = applicazione;
		this.codiceApplicazione = codiceApplicazione;
		this.chiave1 = chiave1;
		this.chiave2 = chiave2;
		this.chiave3 = chiave3;
		this.chiave4 = chiave4;
		this.chiave5 = chiave5;
		this.chiave6 = chiave6;
		this.dataInserimento = dataInserimento;
		this.dataAggiornamento = dataAggiornamento;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIdUnivocoRichiesta() {
		return idUnivocoRichiesta;
	}

	public void setIdUnivocoRichiesta(String idUnivocoRichiesta) {
		this.idUnivocoRichiesta = idUnivocoRichiesta;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getApplicazione() {
		return applicazione;
	}

	public void setApplicazione(String applicazione) {
		this.applicazione = applicazione;
	}

	public String getCodiceApplicazione() {
		return codiceApplicazione;
	}

	public void setCodiceApplicazione(String codiceApplicazione) {
		this.codiceApplicazione = codiceApplicazione;
	}

	public String getChiave1() {
		return chiave1;
	}

	public void setChiave1(String chiave1) {
		this.chiave1 = chiave1;
	}

	public String getChiave2() {
		return chiave2;
	}

	public void setChiave2(String chiave2) {
		this.chiave2 = chiave2;
	}

	public String getChiave3() {
		return chiave3;
	}

	public void setChiave3(String chiave3) {
		this.chiave3 = chiave3;
	}

	public String getChiave4() {
		return chiave4;
	}

	public void setChiave4(String chiave4) {
		this.chiave4 = chiave4;
	}

	public String getChiave5() {
		return chiave5;
	}

	public void setChiave5(String chiave5) {
		this.chiave5 = chiave5;
	}

	public String getChiave6() {
		return chiave6;
	}

	public void setChiave6(String chiave6) {
		this.chiave6 = chiave6;
	}

	public Date getDataInserimento() {
		return dataInserimento;
	}

	public void setDataInserimento(Date dataInserimento) {
		this.dataInserimento = dataInserimento;
	}

	public Date getDataAggiornamento() {
		return dataAggiornamento;
	}

	public void setDataAggiornamento(Date dataAggiornamento) {
		this.dataAggiornamento = dataAggiornamento;
	}

}
