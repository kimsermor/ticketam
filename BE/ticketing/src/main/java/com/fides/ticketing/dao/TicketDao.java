package com.fides.ticketing.dao;

import java.util.List;

import com.fides.ticketing.dto.FE.FullSearch;
import com.fides.ticketing.models.Ticket;

public interface TicketDao {

	public List<Ticket> fullSearch(FullSearch fullSearch);
	
	public List<Ticket> findLastTen();
}
