package com.fides.ticketing.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.fides.ticketing.dao.TicketDao;
import com.fides.ticketing.models.Ticket;

@Repository
public interface TicketRepository extends MongoRepository<Ticket, String>,TicketDao{

	Ticket findByIdUnivocoRichiesta(String idUnivocoRichiesta);
}
