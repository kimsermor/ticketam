package com.fides.ticketing.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.Query;

import com.fides.ticketing.dao.TicketDao;
import com.fides.ticketing.dto.FE.FullSearch;
import com.fides.ticketing.models.Ticket;

public class TicketDaoImpl implements TicketDao {

	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public List<Ticket> fullSearch(FullSearch fullSearch) {
		final Query query = new Query();
		final List<Criteria> criteria = new ArrayList<>();
		if (fullSearch.getStato() != null) {
			criteria.add(Criteria.where("stato").regex(MongoRegexCreator.INSTANCE
					.toRegularExpression(fullSearch.getStato(), MongoRegexCreator.MatchMode.EXACT), "i"));
		}
		if (fullSearch.getFullTextSearch() != null && !fullSearch.getFullTextSearch().isEmpty()) {
			criteria.add(new Criteria().orOperator(
					Criteria.where("stato")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("stato")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("idUnivocoRichiesta")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("text1")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("text2")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("note")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("applicazione")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("codiceApplicazione")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("chiave1")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("chiave2")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("chiave3")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("chiave4")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("chiave5")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("descrizione")
							.regex(MongoRegexCreator.INSTANCE.toRegularExpression(fullSearch.getFullTextSearch(),
									MongoRegexCreator.MatchMode.CONTAINING), "i"),
					Criteria.where("chiave6").regex(MongoRegexCreator.INSTANCE.toRegularExpression(
							fullSearch.getFullTextSearch(), MongoRegexCreator.MatchMode.CONTAINING), "i")

			));
		}
		if (!criteria.isEmpty()) {
			query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
		}
		return mongoTemplate.find(query, Ticket.class);
	}

	@Override
	public List<Ticket> findLastTen() {
		final Query query = new Query();
		query.limit(10);
		query.with(new Sort(Direction.DESC, "dataInserimento"));
		query.addCriteria(new Criteria().andOperator(Criteria.where("stato").ne("CHIUSO"),
				Criteria.where("stato").ne("Chiuso"), Criteria.where("stato").ne("chiuso")));
		return mongoTemplate.find(query, Ticket.class);
	}

}
