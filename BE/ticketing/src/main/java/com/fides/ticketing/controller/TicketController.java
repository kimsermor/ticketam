package com.fides.ticketing.controller;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fides.ticketing.dto.FE.FullSearch;
import com.fides.ticketing.models.Ticket;
import com.fides.ticketing.repository.TicketRepository;
import com.fides.ticketing.service.ExcelService;
import com.fides.ticketing.service.TicketService;

@RestController
@CrossOrigin
public class TicketController {

	Logger logger = LoggerFactory.getLogger(TicketController.class);

	@Autowired
	TicketRepository ticketRepository;

	@Autowired
	ExcelService excelService;

	@Autowired
	TicketService ticketService;

	@GetMapping("/getAllTickets")
	public List<Ticket> getAllTickets() {
		return ticketRepository.findAll();
	}

	@GetMapping("/findLastsTickets")
	public List<Ticket> findLastsTickets() {
		return ticketRepository.findLastTen();
	}

	@GetMapping("/getTicket/{id}")
	public ResponseEntity<Ticket> getTicketById(@PathVariable("id") String id) {
		try {
			return new ResponseEntity<Ticket>(ticketRepository.findByIdUnivocoRichiesta(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/updateTicket/{id}")
	public Ticket updateTicket(@PathVariable("id") String id, @RequestBody Ticket ticket) {
		return ticketRepository.save(ticket);
	}

	@PostMapping("/findFullTickets")
	public List<Ticket> findFullTickets(@RequestBody FullSearch fullSearch) {
		return ticketRepository.fullSearch(fullSearch);
	}

	@PostMapping("/uploadTickets")
	public ResponseEntity<List<Ticket>> uploadTickets(@RequestBody MultipartFile file,
			@RequestParam boolean flagUpdate) {
		List<Ticket> ticketUploaded = null;
		File convFile = null;
		try {
			convFile = new File("uploads.xls");
			FileUtils.writeByteArrayToFile(convFile, file.getBytes());
			ticketUploaded = excelService.readExcelToTickets(convFile);
			ticketService.uploadTickets(ticketUploaded, flagUpdate);
			return new ResponseEntity<List<Ticket>>(ticketUploaded, HttpStatus.OK);
		} catch (Exception ex) {
			logger.error("Service uploadTickets Exception - " + ex.getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			if (convFile != null)
				convFile.delete();
		}
	}

}
