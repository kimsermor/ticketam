package com.fides.ticketing.service;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fides.ticketing.models.Ticket;
import com.fides.ticketing.repository.TicketRepository;

@Service
public class TicketService {
	
	@Autowired
	TicketRepository ticketRepository;
	
	public void uploadTickets(List<Ticket> tickets, boolean flagUpdate) throws IllegalAccessException, InvocationTargetException {
		
		for(Ticket ticket : tickets) {
			Ticket ticketTrovato = ticketRepository.findByIdUnivocoRichiesta(ticket.getIdUnivocoRichiesta());
			if (ticketTrovato!=null && flagUpdate) {
				String idTrovato = ticketTrovato.getId();
				BeanUtils.copyProperties(ticketTrovato, ticket);
				ticketTrovato.setId(idTrovato);
				ticketTrovato.setDataAggiornamento(new Date());
				ticketRepository.save(ticketTrovato);
			} else if (ticketTrovato == null) {
				ticket.setDataInserimento(new Date());
				ticketRepository.save(ticket);
			}
		}
	}
}
