package com.fides.ticketing.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.stereotype.Service;

import com.fides.ticketing.models.Ticket;

import io.github.millij.poi.SpreadsheetReadException;
import io.github.millij.poi.ss.reader.XlsxReader;

@Service
public class ExcelService {

	public List<Ticket> readExcelToTickets(File file) throws IOException, InvalidFormatException, SpreadsheetReadException {
		XlsxReader reader = new XlsxReader();
		List<Ticket> tickets = reader.read(Ticket.class, file);
		return tickets;
	}
}
