import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Ticket } from '../model/ticketing';
import { TicketingService } from '../services/ticketing.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { DatePipe } from '@angular/common';
import { UploadSearch } from '../model/upload-search';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgProgress, NgProgressRef } from '@ngx-progressbar/core';


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})

export class UploadComponent implements OnInit {
  srcResult: any;
  formUpload: FormGroup;
  fileToUpload: File = null;
  flagUpdate: boolean;
  message: string;
  oraAttuale: string;
  action: string;
  fileUpload: UploadSearch;
  progressRef: NgProgressRef;
  constructor(
    public dialogRef: MatDialogRef<UploadComponent>,
    private fb: FormBuilder, private services: TicketingService, private router: Router, private route: ActivatedRoute,
    private _snackBar: MatSnackBar, private datePipe: DatePipe, private spinner: NgxSpinnerService, private progress: NgProgress) { }

  ngOnInit() {
    this.formUpload = this.fb.group({
      file: [null, [Validators.required]],
      flag: ['', []]

    });
    this.progressRef = this.progress.ref('myProgress');
    
  }

  closeDialog() {
    this.dialogRef.close();
  }

  upload() {
    //this.spinner.show();
    this.progressRef.start();
    this.fileUpload = {};
    this.flagUpdate = this.formUpload.value.flag;
    if(!this.flagUpdate){
      this.flagUpdate = false;
    }
    this.fileUpload.flagUpdate = this.flagUpdate;
    this.fileUpload.file = this.fileToUpload;
    this.services.uploadTickets(this.fileUpload.file,this.fileUpload.flagUpdate).subscribe(y => {
      this.openSnackBar(4000);
      this.closeDialog();
      this.progressRef.complete();
      //this.spinner.hide();
    });

  }

  /*onFileChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.file = fileList.item(0);
      this.testData = new FormData();
      this.testData.append('file', this.file, this.file.name);
    // this.formUpload.get('file').setValue(this.file);
    }
  } */
  onFileChange(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  openSnackBar(duration: number) {
    this.message = 'Caricamento file effettuato ';
    this.action = 'Close',
      this._snackBar.open(this.message, this.action, {
        duration: duration,
      });
  }

}
