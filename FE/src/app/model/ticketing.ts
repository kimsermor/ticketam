export interface Ticket {

    id?: string;

    idUnivocoRichiesta?: string;
    
    descrizione?: string;

    note?: string;
    
    stato?: string;
    
    applicazione?: string;

    codiceApplicazione?: string;

    chiave1?: string;

    chiave2?: string;

    chiave3?: string;

    chiave4?: string;

    chiave5?: string;

    chiave6?: string;

    dataInserimento?: Date;
	
	dataAggiornamento?: Date;
}
