import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';
import { Ticket } from '../model/ticketing';
import { TicketingService } from '../services/ticketing.service';
import { FullSearch } from '../model/full-search';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { state, trigger, transition, animate, style } from '@angular/animations';
import { NgxSpinnerService } from 'ngx-spinner';
import { FilterService } from '../services/filter.service';
export interface Stato {
  value: string;
  viewValue: string;
}
export interface RadioButton {
  value: string;
  checked: boolean;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*', minHeight:'20px', background: '#F5F5F5', width: '100%'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['idUnivocoRichiesta','stato', 'codiceApplicazione'];
  dataSource;
  message: string;
  action: string;
  lunghezza: number;
  stati: Stato[] = [
    { value: null, viewValue: null },
    { value: 'Aperto', viewValue: 'Aperto' },
    { value: 'Risolto', viewValue: 'Risolto' },
    { value: 'Pendente', viewValue: 'Pendente'},
    { value: 'In corso', viewValue: 'In corso'},
    { value: 'Annullato', viewValue: 'Annullato'},
    { value: 'Assegnato', viewValue: 'Assegnato'},
    { value: 'Chiuso', viewValue: 'Chiuso'}
  ];

  ricercaValue: RadioButton[] = [{
    value: 'Ricerca per Ticket',
    checked: true
  }, {
    value: 'Ricerca Avanzata',
    checked: false
  }];
  scelta: RadioButton[] = [{
    value: 'Ultimi ticket',
    checked: true
  }, {
    value: 'Visualizza tutti',
    checked: false
  }];
  sceltaStat: string = this.scelta[0].value;
  ricercaStat: string = this.ricercaValue[0].value;
  form: FormGroup;
  dettaglio: FormGroup;
  id: string;
  testo: string;
  stato: string;
  ticket: Ticket;
  ticketList: Array<Ticket>;
  fullSearch: FullSearch;
  disabled: boolean;
  disabledAvanzata: boolean = false;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private services: TicketingService,
    private _snackBar: MatSnackBar,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private router: Router,
    private spinner: NgxSpinnerService,
    private filterService: FilterService) { }

  ngOnInit() {

    this.form = this.fb.group({
      id: ['' , []],
      testo: [ '', []],
      stato: [ '', []],
    });
    
    this.dettaglio = this.fb.group({
      idUni: ['' , []],
      applicazione: ['' , []],
      note: ['' , []],
      chiave1: ['',[]],
      chiave2: ['',[]],
      chiave3: ['',[]],
      chiave4: ['',[]],
      chiave5: ['',[]],
      chiave6: ['',[]],
      descrizione: ['', []],
      dataAggiornamento: ['', []],
      dataInserimento: ['', []],
    });
  
    if(this.ricercaStat ==='Ricerca per Ticket'){
      this.form.get('id').enable();
      this.form.get('testo').disable();
      this.form.get('stato').disable();
    }

    if(this.filterService.sceltaStatoFilter){
      this.sceltaStat= this.filterService.sceltaStatoFilter;
      if(this.sceltaStat === 'Ultimi ticket'){
        this.loadData();
      }
      if(this.sceltaStat === 'Visualizza tutti'){}
      this.getAllTicket();
    }else{
      this.loadData();
    }
    if(this.filterService.id){
     // this.form.get('id').setValue(this.filterService.id);
    }
    
   // this.loadData();
  }
  loadData() {
    this.spinner.show();
    this.services.findLastsTickets().subscribe(res => {
      this.ticketList = res;
      if (this.ticketList) {
        this.lunghezza = this.ticketList.length;
        this.dataSource = new MatTableDataSource<Ticket>(this.ticketList);
        this.dataSource.paginator = this.paginator;
        this.spinner.hide();
      } else {
        this.lunghezza = 0;
        this.openSnackBar(3000);
        this.spinner.hide();
      }
      this.spinner.hide();
    }, error => {
      this.openSnackBar(3000);
    });

  }
  ricercaAvanzata(){
    this.ticketList = [];
    this.id = this.form.get('id').value;
    this.testo = this.form.get('testo').value;
    this.stato = this.form.get('stato').value;
    if(this.stato == ""){
      this.stato = null;
    }
    this.spinner.show();
    if(this.id && (this.testo || this.stato)){
    }else{
      if(this.id){
      this.services.getTicket(this.id).subscribe(res => {
        this.ticket = res;
        if (this.ticket) {
          this.ticketList.push(this.ticket);
          this.lunghezza = this.ticketList.length;
          this.dataSource = new MatTableDataSource<Ticket>(this.ticketList);
          console.log(this.lunghezza);
          this.dataSource.paginator = this.paginator;
          this.spinner.hide();
        } else {
          this.lunghezza = 0;
          this.spinner.hide();
          //this.openSnackBar(3000);
        }
      }, error => {
        this.lunghezza = 0;
        this.spinner.hide();
      });
      }else  if(this.testo || this.stato){
        this.fullSearch = {};
        this.fullSearch.fullTextSearch = this.testo;
        this.fullSearch.stato = this.stato;
        this.services.findFullTickets(this.fullSearch).subscribe(y=>{
          this.ticketList = y;
          if (this.ticketList) {
            this.lunghezza = this.ticketList.length;
            this.dataSource = new MatTableDataSource<Ticket>(this.ticketList);
            this.dataSource.paginator = this.paginator;
            this.spinner.hide();
          } else {
            this.lunghezza = 0;
            //this.openSnackBar(3000);
            this.spinner.hide();
          }
          this.spinner.hide();
        })
        }else if(!(this.id)){
          this.loadData();
        }
    }
   
    }

  openSnackBar(duration: number) {
    this.message = 'Nessun dato da mostrare';
    this.action = 'Close',
      this._snackBar.open(this.message, this.action, {
        duration: duration,
      });
  }

 /* openDialog(event) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = event;
    dialogConfig.minWidth = '500px';
    dialogConfig.minHeight = '500px';
    const dialogRef = this.dialog.open(UpdateModalComponent, dialogConfig);

   /* dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );   
} */
navigateToUpdate(event){
  this.filterService.id = event.idUnivocoRichiesta;
 // console.log(this.filterService.id);
  this.router.navigate(["/update",{ id: event.idUnivocoRichiesta }]);
}
onItemChange(element){
  if(element.value === 'Ricerca Avanzata'){
    this.form.get('id').disable();
    this.form.get('id').setValue('');
    this.form.get('testo').enable();
    this.form.get('stato').enable();
    //this.filterService.ricercaStatoFilter = element.value;
  }
  if(element.value === 'Ricerca per Ticket'){
    
    this.form.get('id').enable();
    
    this.form.get('stato').setValue('');
    this.form.get('testo').setValue('');
    
    this.form.get('testo').disable();
    this.form.get('stato').disable();
    //this.filterService.ricercaStatoFilter = element.value;
  }
}

getAllTicket(){
  this.spinner.show();
  this.services.getAllTickets().subscribe(res => {
    this.ticketList = res;
    if (this.ticketList) {
      this.lunghezza = this.ticketList.length;
      this.dataSource = new MatTableDataSource<Ticket>(this.ticketList);
      this.dataSource.paginator = this.paginator;
      this.spinner.hide();
    } else {
      this.lunghezza = 0;
      this.openSnackBar(3000);
      this.spinner.hide();
    }
    this.spinner.hide();
  }, error => {
    this.openSnackBar(3000);
  });
}

onSceltaChange(element){
  this.dataSource= '';
  if(element.value === 'Visualizza tutti'){
    this.getAllTicket();
    this.filterService.sceltaStatoFilter = element.value;
  }
  if(element.value === 'Ultimi ticket'){
    this.loadData();
    this.filterService.sceltaStatoFilter = element.value;
  }
}
}
