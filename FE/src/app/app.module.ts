import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './navigation/header/header.component';
import { MatGridListModule,MatCheckboxModule,MatCardModule, MatRadioModule, MatDialogModule,MatSnackBarModule,MatNativeDateModule, MatDatepickerModule,MatPaginatorModule,MatToolbarModule,MatTabsModule, MatInputModule, MatSidenavModule,MatIconModule, MatButtonModule,MatListModule,MatTableModule, MatSelectModule  } from '@angular/material';
import { UploadComponent } from './upload/upload.component';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { SidenavListComponent } from './navigation/sidenav-list/sidenav-list.component';7
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TicketingService } from './services/ticketing.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateTicketComponent } from './update-ticket/update-ticket.component';
import { DatePipe } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgProgressModule } from '@ngx-progressbar/core';



 

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    UploadComponent,
    SidenavListComponent,
    UpdateTicketComponent
  ],
  entryComponents: [UploadComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatTabsModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatFileUploadModule,
    MatListModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    HttpClientModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatGridListModule,
    MatCheckboxModule,
    NgxSpinnerModule,
    NgProgressModule.withConfig({
      color: 'white',
      fixed: true,
      meteor: true
    })
  ],
  exports: [
    MatToolbarModule,
    MatTabsModule,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    MatListModule,
    MatDialogModule
  ],
  providers: [TicketingService, DatePipe],
  bootstrap: [AppComponent],
  //entryComponents: [UpdateModalComponent]
})
export class AppModule { }
