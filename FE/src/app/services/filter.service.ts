import { Injectable } from '@angular/core';
import { FullSearch } from '../model/full-search';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  id: string;
  fullSearchFilter: FullSearch;
  ricercaStatoFilter: string;
  sceltaStatoFilter: string;
  constructor() { }
}
