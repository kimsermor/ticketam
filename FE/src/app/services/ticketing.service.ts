import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FullSearch } from '../model/full-search';
import { Ticket } from '../model/ticketing';
import { UploadComponent } from '../upload/upload.component';
import { Observable } from 'rxjs';
import { UploadSearch } from '../model/upload-search';

@Injectable({
  providedIn: 'root'
})
export class TicketingService {

  uri = 'http://localhost:8080/ticketing';

  constructor(private http: HttpClient) { }

  getAllTickets() {
    return this.http.get<Array<Ticket>>(`${this.uri}/getAllTickets`);
  }

  getTicket(id) {
    return this.http.get<Ticket>(`${this.uri}/getTicket/${id}`);
  }

  findFullTickets(fullSearch: FullSearch) {
    return this.http.post<Array<Ticket>>(`${this.uri}/findFullTickets`, fullSearch);
  }
  updateTicket(id, ticket: Ticket) {
    return this.http.put<Ticket>(`${this.uri}/updateTicket/${id}`, ticket);
  }

  uploadTickets(fileToUpload: File, flagUpdate: boolean) {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post<any>(`${this.uri}/uploadTickets?flagUpdate=`+ flagUpdate, formData);
  }
  findLastsTickets() {
    return this.http.get<Array<Ticket>>(`${this.uri}/findLastsTickets`);
  }
}
