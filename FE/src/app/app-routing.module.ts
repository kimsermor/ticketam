import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UploadComponent } from './upload/upload.component';
import { UpdateTicketComponent } from './update-ticket/update-ticket.component';


const routes: Routes = [ {
  path: 'home',
  component: HomeComponent,
},
{
  path: '', redirectTo: 'home', pathMatch: 'full'
},
{
  path: 'upload',
  component: UploadComponent
},
{
  path: 'update',
  component: UpdateTicketComponent
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
