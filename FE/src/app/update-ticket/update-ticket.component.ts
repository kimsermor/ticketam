import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Ticket } from '../model/ticketing';
import { TicketingService } from '../services/ticketing.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { DatePipe } from '@angular/common';
import { FilterService } from '../services/filter.service';
export interface Stato {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-update-ticket',
  templateUrl: './update-ticket.component.html',
  styleUrls: ['./update-ticket.component.css']
})
export class UpdateTicketComponent implements OnInit {
  stati: Stato[] = [
    { value: 'Aperto', viewValue: 'Aperto' },
    { value: 'Risolto', viewValue: 'Risolto' },
    { value: 'Pendente', viewValue: 'Pendente'},
    { value: 'In corso', viewValue: 'In corso'},
    { value: 'Annullato', viewValue: 'Annullato'},
    { value: 'Assegnato', viewValue: 'Assegnato'},
    { value: 'Chiuso', viewValue: 'Chiuso'}
  ];

  form: FormGroup;
  dataPresi: Ticket;
  dataModificati: Ticket;
  id: string;
  message: string;
  oraAttuale: string;
  action: string;
  constructor(private fb: FormBuilder, private services: TicketingService, private router: Router, private route: ActivatedRoute,
    private _snackBar: MatSnackBar, private datePipe: DatePipe, private filterService: FilterService) {
    // this.dataPresi = data;
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.dataPresi = {};
    this.form = this.fb.group({
      id: ['', []],
      idUnivoco: ['', []],
      stato: ['', []],
      note: ['', []],
      chiave1: ['', []],
      chiave2: ['', []],
      chiave3: ['', []],
      chiave4: ['', []],
      chiave5: ['', []],
      chiave6: ['', []],
      descrizione: ['', []],
      codiceApplicazione: ['', []],
      applicazione: ['', []],
      dataAggiornamento: ['', []],
      dataInserimento: ['', []],
    });
    this.services.getTicket(this.id).subscribe(y => {
      this.dataPresi = y;
      this.form = this.fb.group({
        idUnivoco: [this.dataPresi.idUnivocoRichiesta, []],
        stato: [this.dataPresi.stato, []],
        note: [this.dataPresi.note, []],
        chiave1: [this.dataPresi.chiave1, []],
        chiave2: [this.dataPresi.chiave2, []],
        chiave3: [this.dataPresi.chiave3, []],
        chiave4: [this.dataPresi.chiave4, []],
        chiave5: [this.dataPresi.chiave5, []],
        chiave6: [this.dataPresi.chiave6, []],
        descrizione: [this.dataPresi.descrizione, []],
        codiceApplicazione: [this.dataPresi.codiceApplicazione, []],
        applicazione: [this.dataPresi.applicazione, []],
        dataAggiornamento: ['', []],
        dataInserimento: ['', []],
      });
    });
  }
  modifica() {
    this.dataModificati = {};
    const NOW = new Date();
    this.dataModificati = this.dataPresi;
    this.dataModificati.stato = this.form.get('stato').value;
    this.dataModificati.note = this.form.get('note').value;
    this.dataModificati.chiave1 = this.form.get('chiave1').value;
    this.dataModificati.chiave2 = this.form.get('chiave2').value;
    this.dataModificati.chiave3 = this.form.get('chiave3').value;
    this.dataModificati.chiave4 = this.form.get('chiave4').value;
    this.dataModificati.chiave5 = this.form.get('chiave5').value;
    this.dataModificati.chiave6 = this.form.get('chiave6').value;
    this.dataModificati.descrizione = this.form.get('descrizione').value;
    this.dataModificati.codiceApplicazione = this.form.get('codiceApplicazione').value;
    this.dataModificati.applicazione = this.form.get('applicazione').value;
    this.dataModificati.dataAggiornamento = NOW;

    this.services.updateTicket(this.dataPresi.id, this.dataModificati).subscribe(y => {
      this.openSnackBar(4000);
      this.router.navigate(["/home"]);
    });
  }
  openSnackBar(duration: number) {
    this.message = 'Modifica effettuata per il ticket: ' + this.id;
    this.action = 'Close',
      this._snackBar.open(this.message, this.action, {
        duration: duration,
      });
  }
  indietro() {
    this.filterService.id = '';
    this.router.navigate(["/home"]);
  }
}